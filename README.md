# [](http://.org) - Közösségi tábla alapú szerepjáték világ

XXXX ([XXXXX.org](http://.org)) földje ma még titokzatos új világ. Évekkel ezelőtt fedezte fel [Senkiur](http://.org/users/senkiur) aki a feljegyzések alapján egy komplett, addig elzárt városra lelt Óperencia északi partjainál.

**Figyelem**: A **játékhoz csatlakozni** az alfa időszak alatt kizárólag személyes meghívás útján lehetséges. **Béta útlevelet** kaphatsz azonban, ha ellátogatsz az itteni kikötőbe: [Az új világba tartó hajók](https://.org/beta-keys) Egy hajón legfeljebb 50 ember fér el biztonságosan, 2017 nyarán előreláthatólag 3 hajónyi ember nyerhet bebocsátást.

## Mi vár rám ha csatlakozok?

- **Minden a döntéseiden múlik**. Az új világba érkezők teljesen egyedi módon a saját döntéseik hatására kapnak küldetéseket, alakíthatják sorsukat.
- **Idő alapú játékmechanika**. Az idő ugyanúgy telik mint bárhol máshol, rajtad múlik hogyan használod fel.
- **Sötét középkor**. 
- **Hatalmi rendszer**. Vedd kezedbe az irányítást! A játékszabályokat XXXXX uralkodó rétege tartja kézben. Nincsenek MODok vagy tejhatalmú fejlesztők, csak nemesi réteg, céh vezérek, egyház és az uralkodó ház.

## Technológia

- **Multiplatform** A játék böngésző alapú, bármilyen eszközről játszhatsz vele.
- **Tiszta valósidejűség** A websocket technológia miatt valós időben szinkronizálva történik minden.
- **Desktop minőség** A Reactnak hála a játék nagyon gyors.
- **3D látványelemek** ThreeJS-el készültek a játékot dekoráló 3D-s elemek.
- **Rád szabva**: A játék magvát egy dinamikus [személyiség mérő](http://.org) algoritmus alkotja.

## Questions?

- Explore the complete [documentation](http://phantomjs.org/documentation/).
- Read tons of [user articles](http://phantomjs.org/buzz.html) on using PhantomJS.
- Join the [mailing-list](http://groups.google.com/group/phantomjs) and discuss with other PhantomJS fans.

PhantomJS is free software/open source, and is distributed under the [BSD license](http://opensource.org/licenses/BSD-3-Clause). It contains third-party code, see the included `third-party.txt` file for the license information on third-party code.

PhantomJS is created and maintained by [Ariya Hidayat](http://ariya.ofilabs.com/about) (Twitter: [@ariyahidayat](http://twitter.com/ariyahidayat)), with the help of [many contributors](https://github.com/ariya/phantomjs/contributors). Follow the official Twitter stream [@PhantomJS](http://twitter.com/PhantomJS) to get the frequent development updates.
