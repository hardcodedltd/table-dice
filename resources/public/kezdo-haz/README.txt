A Pen created at CodePen.io. You can find this one at http://codepen.io/Hornebom/pen/OPEbMV.

 /* This is for Chrome only */

This pen is the result of two inspirations. First there's this great article by Hugo Giraudel on CSS-Tricks about the CSS property perspective-origin (https://css-tricks.com/almanac/properties/p/perspective-origin/). Second inspiration is a surrealistic artwork by Max Ernst named "The Master's Bedroom" (http://images.huffingtonpost.com/2013-06-09-themastersbedroom.jpg).

Actually it would be quite easy to change the perspective origin using js or jQuery + using 100 divs without any content doesn't make sense. But hey, writing nested loops in SCSS is definitely much fun on a friday night...