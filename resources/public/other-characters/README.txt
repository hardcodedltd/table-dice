A Pen created at CodePen.io. You can find this one at http://codepen.io/arelia/pen/bVNxgx.

 Recreating the interface from the 2014 Monopoly online game using flexbox, jQuery, CSS animations and Sass. Just for fun. Trying to improve on the animation.

When you click on a "game piece" it expands at the top. When you click on another game piece or click the expanded area, that color goes back into it's original position.

See the original here: https://web.archive.org/web/20141109061810/http://www.playatmcd.com/