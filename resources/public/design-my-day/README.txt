A Pen created at CodePen.io. You can find this one at http://codepen.io/joacimnilsson/pen/ojZeYZ.

 I wanted to visualise my day and here is the result. You will see two axis. The top is for the hours of the day and the left is the thing that is a part of your time. Each hour is divided by three so 20 minutes each hour.

Click to select a 20 minute span or click and drag to select multiple at the same time. Add new things and remove the ones you don't need. Good luck :D

http://joacimnilsson.se/